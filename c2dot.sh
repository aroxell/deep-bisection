#!/bin/bash

COMMITS_FILE="${1:-commits.txt}"

ctr=0
# shellcheck disable=SC2002
max_lines="$(cat "${COMMITS_FILE}" | wc -l)"

echo "digraph BST {"

# shellcheck disable=SC2094
while read -r commit; do
  ctr=$((ctr + 1))
  unset color
  if [ -e "${commit}/status.txt" ]; then
    # shellcheck disable=SC2002
    color="$(cat "${commit}/status.txt" | sed -e 's:fail:red:g' -e 's:pass:green:g')"
  fi
  if [ -n "${color}" ]; then
    echo "  \"${commit:0:12}\" [fillcolor = ${color}, style=filled];"
  else
    echo "  \"${commit:0:12}\";"
  fi

  line_if_old=$(((ctr * 2) + 0))
  line_if_new=$(((ctr * 2) + 1))
  if [ "${line_if_old}" -lt "${max_lines}" ]; then
    # shellcheck disable=SC2094
    old="$(sed -e "${line_if_old}!d" "${COMMITS_FILE}")"
    # shellcheck disable=SC2094
    new="$(sed -e "${line_if_new}!d" "${COMMITS_FILE}")"

    echo "  \"${commit:0:12}\" -> \"${old:0:12}\" [label = \"old\"];"
    echo "  \"${commit:0:12}\" -> \"${new:0:12}\" [label = \"new\"];"
  fi
done < "${COMMITS_FILE}"
echo "}"
