#!/usr/bin/env python3

import argparse
import subprocess

# TODO: validate git revisions exists
# TODO: validate output of subprocess
# NOTE: Maybe use git library
def get_next(old, new, checked=[]):
    # cmd = f"git -C {args.git_dir} rev-list --bisect-all {old}..{new}".split(" ")
    cmd = f"git -C {args.git_dir} rev-list --bisect {old}..{new}".split(" ")
    if len(checked) > 0:
        cmd.append("--not")
        cmd += checked
    # print ("[cmd] %s" % str(cmd))
    output = subprocess.check_output(cmd).decode()
    for line in output.splitlines():
        return line[0:40]
    return


def get_candidates(old, new, excluded, current, depth=1):
    if_current_is_old = get_next(old, new, checked=excluded + [current])
    if_current_is_new = get_next(old, current, checked=excluded)
    revs = [if_current_is_old, if_current_is_new]
    if depth > 1:
        # old
        revs += get_candidates(
            old, new, excluded + [current], if_current_is_old, depth - 1
        )
        # new
        revs += get_candidates(old, current, excluded, if_current_is_new, depth - 1)
    # print({args.depth - depth: [revs]})
    return revs


# $0 -o v4.14.216                                -n linux-stable-rc/linux-4.14.y             -d 0
# $0 -o 2762b48e9611529239da2e68cba908dbbec9805f -n 5f9d9acc3e9d6464861aab91dede5a15e7a6c428 -d 0
# $0 -o 2762b48e9611529239da2e68cba908dbbec9805f -n 5f9d9acc3e9d6464861aab91dede5a15e7a6c428 -d 1
# $0 -o 2762b48e9611529239da2e68cba908dbbec9805f -n 5f9d9acc3e9d6464861aab91dede5a15e7a6c428 -d 2
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Git advanced bisection helper")
    parser.add_argument("-o", "--old", required=True, help="Revision with old behavior")
    parser.add_argument("-n", "--new", required=True, help="Revision with new behavior")
    parser.add_argument(
        "-d", "--depth", default=1, type=int, help="Revision with old behavior"
    )
    parser.add_argument(
        "-e", "--exclude", nargs="+", help="Revision to exclude due to old behavior"
    )
    parser.add_argument(
        "-C", "--git-dir", default=".", help="Directory to Git repo to bisect"
    )

    args = parser.parse_args()

    excluded = args.exclude or []

    current = get_next(args.old, args.new, checked=excluded)
    print(current)

    # NOTE: Does not work well for depth >= 3
    if args.depth >= 1:
        for rev in get_candidates(args.old, args.new, excluded, current, args.depth):
            print(rev)
