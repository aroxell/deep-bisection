#!/bin/bash

show_help() {
  echo "$0 -C /path/to/kernel -o OLD [-n NEW] -t https://tuxbuild.url/Ab1Cd2Ef3Gh4/"
  echo
  echo "If a NEW revision is passed, that one will be used for the bisection, instead"
  echo "of the one identified by the tuxsuite_reproducer.sh (from the Tuxbuild URL)."
}

get_bisection_steps() {
  old=$1
  shift
  new=$1
  shift
  if [ $# -ge 3 ]; then
    bisection_steps=$(git -C "${GIT_DIR}" rev-list --bisect-vars "${old}..${new}" --not "$@" | grep bisect_steps | cut -d= -f2)
  else
    bisection_steps=$(git -C "${GIT_DIR}" rev-list --bisect-vars "${old}..${new}" | grep bisect_steps | cut -d= -f2)
  fi

  if [ -z "${bisection_steps}" ]; then
    bisection_steps=0
  fi
  echo "${bisection_steps}"
}

run_tuxsuite() {
  tuxsuite_command="$(grep ^tuxsuite "${work_dir}/tuxsuite_reproducer.sh")"
  SHA_IN_REPRODUCER="$(grep -Eo -e '--git-sha [0-9a-f]{40}' "${work_dir}/tuxsuite_reproducer.sh" | cut -d\  -f2)"
  # shellcheck disable=SC2001
  cmd="$(echo "${tuxsuite_command}" | sed -e "s:${SHA_IN_REPRODUCER}:$1:g") --json-out build.json"
  # echo "[cmd] ${cmd}"
  (
    $cmd
    jq -r .build_status build.json > status.txt
    touch "done"
  ) &
}

run_tuxsuite_and_test() {
  tuxsuite_command="$(grep ^tuxsuite "${work_dir}/tuxsuite_reproducer.sh")"
  SHA_IN_REPRODUCER="$(grep -Eo -e '--git-sha [0-9a-f]{40}' "${work_dir}/tuxsuite_reproducer.sh" | cut -d\  -f2)"
  # shellcheck disable=SC2001
  cmd="$(echo "${tuxsuite_command}" | sed -e "s:${SHA_IN_REPRODUCER}:$1:g") --json-out build.json"
  # echo "[cmd] ${cmd}"
  (
    $cmd;
    "${root_dir}/lava-job-reproduce.sh" build.json "${LAVA_JOB}" > newjob.yaml
    lavajob_id="$(lavacli jobs submit newjob.yaml)"
    lavacli jobs show "${lavajob_id}"
    while true; do
      lavacli jobs wait "${lavajob_id}"
      # shellcheck disable=SC2181
      if [ $? -eq 0 ]; then break; fi
    done

    # TODO: Use an external discriminator
    lavacli jobs logs "${lavajob_id}" > lava.log
    lavacli results "${lavajob_id}" lava 0_prep-tmp-disk > status.txt
    #lavacli jobs show "${lavajob_id}" | grep ^Health | cut -c 15- > job.status
    #cat job.status | sed -e "s#Incomplete#fail#" -e "s#Complete#pass#" > status.txt
    touch "done"
  ) &
}

root_dir="$(dirname "$(readlink -e "$0")")"
work_dir="$(readlink -e "$(pwd)")"

# Defaults
GIT_DIR="."
LAVA_JOB=""
NEW=""
OLD=""
TUXBUILD_URL=""

# Override defaults with arguments:
while getopts "C:hl:n:o:t:" arg; do
  case ${arg} in
    C)
      GIT_DIR="${OPTARG}"
      ;;
    h)
      show_help
      exit 0
      ;;
    l)
      LAVA_JOB="${OPTARG}"
      ;;
    n)
      NEW="${OPTARG}"
      ;;
    o)
      OLD="${OPTARG}"
      ;;
    t)
      TUXBUILD_URL="$(echo "${OPTARG}" | cut -d/ -f1-4)"
      ;;
    *)
      show_help
      exit 1
      ;;
  esac
done
shift $((OPTIND - 1))

if [ -z "${OLD}" ]; then
  show_help
  exit 1
fi

if [ -n "${TUXBUILD_URL}" ]; then
  if [ ! -e "${work_dir}/tuxsuite_reproducer.sh" ]; then
    wget "${TUXBUILD_URL}/tuxsuite_reproducer.sh" -O "${work_dir}/tuxsuite_reproducer.sh"
  fi
fi

if [ -z "${NEW}" ]; then
  NEW="$(grep -Eo -e '--git-sha [0-9a-f]{40}' "${work_dir}/tuxsuite_reproducer.sh" | cut -d\  -f2)"
fi

declare -a excluded
EXCLUDED=""

steps_executed=0
while true; do
  steps_executed=$((steps_executed + 1))
  pending_steps="$(get_bisection_steps "${OLD}" "${NEW}" "${excluded[@]}")"
  if [ "${pending_steps}" -eq 0 ] || [ "${steps_executed}" -gt 20 ]; then
    break
  fi
  set +x

  old_sha="$(git -C "${GIT_DIR}" rev-parse "${OLD}")"
  new_sha="$(git -C "${GIT_DIR}" rev-parse "${NEW}")"

  echo "##### INFO"

  echo "OLD: ${OLD} (${old_sha})"
  echo "NEW: ${NEW} (${new_sha})"
  echo "Bisection steps: ${pending_steps}"

  echo
  echo "##### STEP #${steps_executed}"

  depth=$((pending_steps - 1))
  if [ "${depth}" -gt 2 ]; then
    depth=2
  fi

  if [ "${depth}" -lt 0 ]; then
    depth=0
  fi

  # Forget about depth -- do a regular bisection
  depth=0

  echo "[cmd] ${root_dir}/get-bisection-revs.py -C ${GIT_DIR} -o ${OLD} -n ${NEW} -d ${depth} $(printf "%s" "${EXCLUDED}")"
  ( 
    #    echo "${old_sha}"
    #    echo "${new_sha}"
    # shellcheck disable=SC2046
    "${root_dir}/get-bisection-revs.py" -C "${GIT_DIR}" -o "${OLD}" -n "${NEW}" -d "${depth}" $(printf "%s" "${EXCLUDED}")
  ) > commits.txt
  cp -p commits.txt "commits${steps_executed}.txt"

  # shellcheck disable=SC2002
  total_commits="$(cat commits.txt | wc -l)"

  # Build all suspicious commits (including $OLD + $NEW)
  sort -u commits.txt | while read -r commit; do
    mkdir -p "${commit}"
    # shellcheck disable=SC2164
    cd "${commit}"
    if [ ! -e "done" ]; then
      if [ -z "${LAVA_JOB}" ]; then
        run_tuxsuite "${commit}"
      else
        run_tuxsuite_and_test "${commit}"
      fi
    fi
    # shellcheck disable=SC2103
    cd ..
  done

  # Wait for tuxsuite completion
  while true; do
    ctr=0
    while read -r commit; do
      [ -e "${commit}/done" ] && ctr=$((ctr + 1))
    done < <(sort -u commits.txt)
    if [ "${ctr}" == "${total_commits}" ]; then
      break
    fi
    sleep 30
  done

  #  # Verify OLD and NEW
  #  old_status="$(jq -r .build_status "${old_sha}/build.json")"
  #  new_status="$(jq -r .build_status "${new_sha}/build.json")"
  #
  #  if [ "${old_status}" == "fail" ]; then
  #    echo "WARNING: ${OLD} was expected to pass but it failed!"
  #  fi
  #
  #  if [ "${new_status}" == "pass" ]; then
  #    echo "WARNING: ${NEW} was expected to fail but it passed!"
  #  fi

  # find out new NEW
  echo "Old NEW was ${NEW}"
  while read -r commit; do
    status="$(cat "${commit}/status.txt")"
    if [ "${status}" == "fail" ]; then
      NEW=${commit}
      break
    fi
  done < <(tac commits.txt)
  echo "New NEW is  ${NEW}"

  while read -r commit; do
    if [ "${commit}" == "${NEW}" ]; then
      break
    fi
    status="$(jq -r .build_status "${commit}/build.json")"
    if [ "${status}" == "pass" ]; then
      EXCLUDED="${EXCLUDED} -e ${commit}"
      excluded+=("${commit}")
    fi
  done < <(tac commits.txt)
  # echo "Excluded: ${EXCLUDED}"
done

if [ "${pending_steps}" -eq 0 ]; then
  echo "Bisection done!"
  echo "First commit presenting NEW behaviour: $NEW"
  echo
  git -C "${GIT_DIR}" show "${NEW}"
else
  echo "[cmd] ${root_dir}/get-bisection-revs.py -C ${GIT_DIR} -o ${OLD} -n ${NEW} -d ${depth} $(printf "%s" "${EXCLUDED}")"
  # shellcheck disable=SC2046
  "${root_dir}/get-bisection-revs.py" -C "${GIT_DIR}" -o "${OLD}" -n "${NEW}" -d "${depth}" $(printf "%s" "${EXCLUDED}")
fi
