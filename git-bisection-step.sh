#!/bin/bash

root_dir="$(dirname "$(readlink -e "$0")")"
work_dir="$(readlink -e "$(pwd)")"
git_dir=/data/linux
# shellcheck disable=SC2153
if [ -n "${GIT_DIR}" ]; then
  git_dir="$(readlink -e "${GIT_DIR}/..")"
fi

run_tuxsuite() {
  tuxsuite_command="$(grep ^tuxsuite "${work_dir}/tuxsuite_reproducer.sh")"
  SHA_IN_REPRODUCER="$(grep -Eo -e '--git-sha [0-9a-f]{40}' "${work_dir}/tuxsuite_reproducer.sh" | cut -d\  -f2)"
  # shellcheck disable=SC2001
  cmd="$(echo "${tuxsuite_command}" | sed -e "s:${SHA_IN_REPRODUCER}:$1:g") --json-out build.json"
  # echo "[cmd] ${cmd}"
  (
    $cmd
    jq -r .build_status build.json > status.txt
    touch "done"
  )
}

run_tuxsuite_and_test() {
  tuxsuite_command="$(grep ^tuxsuite "${work_dir}/tuxsuite_reproducer.sh")"
  SHA_IN_REPRODUCER="$(grep -Eo -e '--git-sha [0-9a-f]{40}' "${work_dir}/tuxsuite_reproducer.sh" | cut -d\  -f2)"
  # shellcheck disable=SC2001
  cmd="$(echo "${tuxsuite_command}" | sed -e "s:${SHA_IN_REPRODUCER}:$1:g") --json-out build.json"
  # echo "[cmd] ${cmd}"
  (
    $cmd;
    "${root_dir}/lava-job-reproduce.sh" build.json "${LAVA_JOB}" > newjob.yaml
    lavajob_id="$(lavacli jobs submit newjob.yaml)"
    lavacli jobs show "${lavajob_id}"
    while true; do
      lavacli jobs wait "${lavajob_id}"
      # shellcheck disable=SC2181
      if [ $? -eq 0 ]; then break; fi
    done

    # TODO: Use an external discriminator
    lavacli jobs logs "${lavajob_id}" > lava.log
    lavacli results "${lavajob_id}" lava 0_prep-tmp-disk > status.txt
    #lavacli jobs show "${lavajob_id}" | grep ^Health | cut -c 15- > job.status
    #cat job.status | sed -e "s#Incomplete#fail#" -e "s#Complete#pass#" > status.txt
    touch "done"
  ) &
}


commit="$(git -C "${git_dir}" rev-parse HEAD)"

if [ ! -e "${commit}/done" ]; then
  mkdir -p "${commit}"
  # shellcheck disable=SC2164
  cd "${commit}"
  if [ ! -e "done" ]; then
    if [ -z "${LAVA_JOB}" ]; then
      run_tuxsuite "${commit}"
    else
      run_tuxsuite_and_test "${commit}"
    fi
  fi
  # shellcheck disable=SC2103
  cd ..
fi

# shellcheck disable=SC2002
this_result=$(cat "$(git -C "${git_dir}" rev-parse HEAD)/status.txt" | sed -e 's:fail:new:g' -e 's:pass:old:g')
git -C "${git_dir}" bisect "${this_result}"
